package issues;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class InitDriver {

    public WebDriver driver;

//    public InitDriver(WebDriver driver){
//        this.driver = driver;
//        PageFactory.initElements(driver, this);
//    }

    public void openBrowser(){
        System.setProperty("webdriver.gecko.driver","C:\\Softwares\\geckodriver-v0.23.0-win64\\geckodriver.exe");
        driver = new FirefoxDriver();

    }

    public void waitForPageLoad(int time ){

        driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
    }


    public static void main(String[] args){

        System.setProperty("webdriver.gecko.driver","C:\\Softwares\\geckodriver-v0.23.0-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.facebook.com");
    }
}
