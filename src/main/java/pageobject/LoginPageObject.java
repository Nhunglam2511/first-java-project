package pageobject;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPageObject {

    @FindBy(id = "signup")
    private WebElement signUpLink;

    @FindBy(id="usernamelabel")
    private WebElement usernameLable;

    @FindBy(id="passwordlabel")
    private WebElement passwordLable;

    @FindBy (id="login-form-username")
    private WebElement usernameTxt;

    @FindBy (id="login-form-password")
    private WebElement passwordTxt;

    @FindBy (name ="login")
    private  WebElement logInButton;

    @FindBy (id ="login-form-remember-me")
    private  WebElement remenberMeCheckbox;

    @FindBy (id ="remembermelabel")
    private  WebElement remenberMeLable;

    @FindBy (id ="forgotpassword")
    private  WebElement forgetPasswordLink;

    @FindBy (id ="publicmodeonmsg")
    private  WebElement notMemberLable;

    public LoginPageObject(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isSignUpBlueColor(){
        String color = signUpLink.getCssValue("color").substring(4,14);

        return color.equals("0, 82, 204");
    }

    public void clickSignUp(){
        signUpLink.click();
    }

    public boolean isContainUsername(){
        return usernameLable.getText().equals("Username");
    }

    public boolean isContainPassword(){
        return passwordLable.getText().equals("Password");
    }
    public boolean isContainLogInButton(){
        return logInButton.getAttribute("value").equals("Log In");
    }

    public boolean isRememberCheckboxExist(){

        return remenberMeCheckbox.getAttribute("type").equals("checkbox");
    }

    public boolean isContainRememberLable(){

        return remenberMeLable.getText().equals("Remember my login on this computer");
    }

    public boolean isContainForgetPasswordLink(){

        return forgetPasswordLink.getText().equals("Can't access your account?");
    }

    public boolean isContainNotMemberLable(){
      // System.out.println(notMemberLable.getText());

        return notMemberLable.getText().equals("Not a member? Sign up for an account.");

    }

    public void clickRememberMe(){
        if(!remenberMeCheckbox.isSelected())
            remenberMeCheckbox.click();
    }

    public void enterUsername(String userName){
        usernameTxt.clear();
        usernameTxt.sendKeys(userName);
    }

    public void enterPassword(String password){
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
    }

    public void login(String username, String password){
        usernameTxt.clear();
        usernameTxt.sendKeys(username);

        passwordTxt.clear();
        passwordTxt.sendKeys(password);

        logInButton.click();
    }
    public void clickLogInButton(){
        logInButton.click();
    }
}
