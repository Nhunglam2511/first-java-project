package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class HomePage {
    public HomePage(WebDriver driver){PageFactory.initElements(driver,this);}

    @FindBy (id = "add-gadget")
    private WebElement addGadgetButton;

    @FindBy (id = "gadget-dialog")
    private WebElement gadgetDialog;

    @FindBy (id = "Search")
    private WebElement searchGadgetDialog;

    @FindBys ({@FindBy(className = "add-dashboard-item-selector")})
    private List<WebElement> addGadgetButtonOnDialog;

    @FindBy (css = ".aui-dialog2-header-close")
    private WebElement closeDialog;

    @FindBy (css = ".layout.layout-aa + div .gadget-menu .aui-dd-trigger")
    private WebElement gadgetMenu;

    @FindBy (id = "10125-project-filter-picker")
    private WebElement searchGadget;

    @FindBy (css = ".item-link.delete")
    private WebElement deleteGadgetLink;

    @FindBy (css = ".layout.layout-aa + div .gadget.color1")
    private WebElement existGadgetform;

    @FindBys({@FindBy(css = ".gadget.color1")})
    private List<WebElement> gagets;


    public void clickAddGadgetButton(){

        addGadgetButton.click();
    }

    public boolean verifyGadgetDialogAppeared(){

       return gadgetDialog.getAttribute("aria-hidden").equals("false");
    }


    public void clickAddGadgetButtonOnDialog(){

        addGadgetButtonOnDialog.get(0).click();
    }


    public void clickCloseDialog(){

        closeDialog.click();
    }

    public void clickCloseAddedGadget(){

        gadgetMenu.click();

    }




    public List<WebElement> getExistedGadgets(){
       return gagets;
    }

    public void clickGadgetMenu(WebElement gadget) {
        gadget.findElement(By.cssSelector(".gadget-menu .aui-dd-trigger")).click();
    }

    public void clickDeletGadgetLink(WebElement gadget){
        gadget.findElement(By.cssSelector(".item-link.delete")).click();
    }
}
