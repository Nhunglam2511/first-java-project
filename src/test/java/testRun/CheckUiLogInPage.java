package testRun;

import issues.InitDriver;
import pageobject.LoginPageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

//import static javafx.scene.paint.Color.rgb;


public class CheckUiLogInPage extends InitDriver{



    @Before
    public void goToLogInPage(){
        openBrowser();
        driver.get("https://mobile.bb.atl-test.space/jira/testing");
        driver.manage().window().maximize();
    }




    @Test
    public void checkUiLogInPage(){
       // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        waitForPageLoad(10);
        LoginPageObject pageLogIn = new LoginPageObject(driver);
        assertTrue(pageLogIn.isContainUsername());
        assertTrue(pageLogIn.isContainPassword());
        assertTrue(pageLogIn.isContainLogInButton());
        assertTrue(pageLogIn.isRememberCheckboxExist());
        assertTrue(pageLogIn.isContainRememberLable());
        assertTrue(pageLogIn.isContainForgetPasswordLink());
        assertTrue(pageLogIn.isContainNotMemberLable());

    }

    @Test
    public  void checkSignUpPage(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LoginPageObject page = new LoginPageObject(driver);

        assertTrue(page.isSignUpBlueColor());
        page.clickSignUp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        assertEquals("Sign up for Jira - Jira (Testing)",driver.getTitle());
    }

    @After
    public void closeBrowser(){
        driver.quit();
    }

}
