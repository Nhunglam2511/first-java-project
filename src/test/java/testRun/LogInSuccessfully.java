package testRun;

import issues.InitDriver;
import org.junit.Before;
import org.junit.Test;
import pageobject.LoginPageObject;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LogInSuccessfully extends InitDriver {

    @Before
    public void goToLogInPage(){
        openBrowser();
        driver.get("https://mobile.bb.atl-test.space/jira/testing");
        driver.manage().window().maximize();
    }


    @Test
    public void loginSuccessfully(){

        waitForPageLoad(10);

        LoginPageObject login = new LoginPageObject(driver);
        login.enterUsername("kpham");
        login.enterPassword("kpham");
        login.clickLogInButton();
        waitForPageLoad(10);

        assertEquals("RES - Jira (Testing)", driver.getTitle());
        assertEquals("https://mobile.bb.atl-test.space/jira/testing/secure/Dashboard.jspa",
        driver.getCurrentUrl());

    }

    

}
