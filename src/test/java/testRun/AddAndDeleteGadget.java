package testRun;

import issues.InitDriver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageobject.HomePage;
import pageobject.LoginPageObject;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class AddAndDeleteGadget extends InitDriver {

    @Before
    public void goToLogInPage(){
        openBrowser();
        driver.get("https://mobile.bb.atl-test.space/jira/testing");
        driver.manage().window().maximize();
    }

    @Test
    public void verifyAddGadgetForm(){
        final LoginPageObject loginPageObject = new LoginPageObject(driver);
        final HomePage homePage = new HomePage(driver);

        loginPageObject.login("kpham","kpham");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        homePage.getExistedGadgets()
                .forEach(webElement -> deleteGadget(homePage, webElement));

        homePage.clickAddGadgetButton();
        assertTrue(homePage.verifyGadgetDialogAppeared());

        homePage.clickAddGadgetButtonOnDialog();

        homePage.clickCloseDialog();

        driver.navigate().refresh();
    }

    private void deleteGadget(final HomePage homePage, final WebElement gadget) {
        homePage.clickGadgetMenu(gadget);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        while (!isElementExist(".item-link.delete")) {
            homePage.clickGadgetMenu(gadget);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

        homePage.clickDeletGadgetLink(gadget);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        checkAlertException();
        //driver.navigate().refresh();
    }

    private void checkAlertException(){

        try {
            driver.switchTo().alert().accept();
        } catch (Exception ex){
            System.out.println(ex);
        }
    }

    private boolean isElementExist(String css){
        return   driver.findElements(By.cssSelector(css)).size() > 0;
    }

    @After
    public void closeBrowser(){
        driver.quit();
    }



}
